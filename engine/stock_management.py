from models import Stock


class BadUserException(Exception):
    pass


class GetStockCount:
    def __init__(self):
        pass

    def execute(self, stock: Stock):
        if stock is None:
            raise BadUserException()
        stock_count = Stock.query.count()

        return stock_count


class ComputeStockPrice:
    def __init__(self):
        pass

    def execute(self, stock_price, stock_remaining):
        remaining_value = stock_price * stock_remaining
        return remaining_value
