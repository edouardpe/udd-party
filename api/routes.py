"""users routes"""
import operator
from collections import OrderedDict

from models.db import db
from models import Stock
from flask import current_app as app, jsonify, request


@app.route("/health", methods=["GET"])
def compute():
    app.logger.info("Get Heathcheck")
    result = "Server up and running"

    return jsonify(result), 200


@app.route("/stocks", methods=["GET"])
def get_stock():
    stocks = Stock.query.count()

    return jsonify(stocks)


@app.route("/stocks/add", methods=["GET"])
def add_stock():
    new_stock = Stock()
    new_stock.price = 50
    db.session.add(new_stock)
    db.session.commit()

    return jsonify("ok")