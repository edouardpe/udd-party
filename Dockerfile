FROM python:3

ENV PYTHONUNBUFFERED 1
WORKDIR /usr/local/bin
COPY . .
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python app.py
