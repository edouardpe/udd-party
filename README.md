#TP
Yeah c’est le début du projet dans une toute nouvelle équipe !

On est au sprint 0, l’équipe a choisi de mettre en place un gitlab comme gestionnaire de source.

L’équipe n’est pas encore bien sensibilisée à l’utilisation des tests, il leur arrive d’oublier de les lancer avant de push sur le master…

Ah… Si seulement le linter et les tests étaient lancés automatiquement…
 

Le projet est encore petit mais quand il grossira et qu’on ira en prod ce sera la catastrophe, si on casse quelquechose et qu’on ne l’a pas vu : Indisponibilité du site => perte d’argent => client pas content

## Exercice 1


Vous proposez alors de mettre une CI en place ! Et vous avez justement entendu parlé de GitlabCI.

Vous clonez le repo à l’adresse suivante : git@gitlab.com:edouardpe/udd-party.git ou https://gitlab.com/edouardpe/udd-party.git

Et c’est parti ! Mettons en place notre CI. A chaque fois qu’un développement sera fait et poussé sur n’importe quelle branche, on souhaite pouvoir lancer un linter et des tests.


##############################################

## Exercice 2

Cool, on a notre CI en place.
Maintenant Vous souhaitez pouvoir packager votre application dans une image docker

Vous voyez un magnifique Dockerfile développé par l’équipe, vous allez pouvoir vous en servir pour packager l'application !

On rajoute une nouvelle étape où l’on package notre application dans une image Docker et où l’on pousse cette image sur la registry associée à notre projet gitlab. Pour vous aider deux variables ont été renseignées dans les variables CI/CD sur gitlab CI_GITLAB_REGISTRY_USER et CI_GITLAB_REGISTRY_PWD.

 On ne veut générer cette image qu’au moment de push sur votre branche *teamx*. Les devs ne font leurs tests sur l’environnement local et rien ne sert d’avoir des images qui ne correspondent pas à ce que l’on veut délivrer.

## Exercice 3

Notre application est maintenant packagée, il ne nous reste plus qu’à la déployer sur un environnement .


Un script est disponible dans le projet !

 Pour vous aider deux variables ont été renseignées dans les variables CI/CD sur gitlab AWS_SECRET_ACCESS_KEY, et AWS_ACCESS_KEY_ID

Rajoutons une nouvelle étape à notre CI pour pousser sur un environnement
