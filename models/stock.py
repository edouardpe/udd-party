""" stock """
from datetime import datetime, timedelta
from sqlalchemy import BigInteger, \
                       CheckConstraint, \
                       Column, \
                       DateTime, \
                       DDL, \
                       event, \
                       ForeignKey, \
                       Integer, \
                       Numeric

from models.db import Model


class Stock(Model):

    id = Column(Integer,
                primary_key=True,
                autoincrement=True)

    # an stock is either linked to a thing or to an eventOccurrence

    dateModified = Column(DateTime,
                          nullable=False,
                          default=datetime.utcnow)


    quantity = Column(Integer, nullable=True)

    price = Column(Numeric(10, 2), nullable=False)

    stockMax = Column(Integer, nullable=True)
