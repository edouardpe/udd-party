import pytest

import requests as req

API_URL = "http://api:5000"

@pytest.mark.standalone
def test_health_url_should_be_available():
    # Given

    # When
    r_signup = req.get(API_URL + '/health')

    # Then
    assert r_signup.status_code == 200

