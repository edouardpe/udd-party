import pytest
from engine.stock_management import ComputeStockPrice


@pytest.mark.standalone
def test_compute_stock_price_should_return_0_when_no_stock_remaining():
    # Given
    stock_price = 10
    stock_remaining = 0

    # When
    result = ComputeStockPrice().execute(stock_price=stock_price, stock_remaining=stock_remaining)

    # Then
    assert result == 0

