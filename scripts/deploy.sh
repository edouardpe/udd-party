# Install AWS Command Line Interface
# more details https://aws.amazon.com/cli/
apk add --update python python-dev py-pip
pip install awscli --upgrade


# Set AWS config variables used during the AWS get-login command below
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
export AWS_REGION=eu-west-1
export TEAM=team1

aws sts get-caller-identity
# se connecter à ECS du bon environnement

# push le app --> tache / service MaJ

aws ecs register-task-definition \
  --family FORM-UDD-"$TEAM"_app \
  --network-mode awsvpc \
  --requires-compatibilities FARGATE \
  --region $AWS_REGION \
  --cpu 256 \
  --memory 512 \
  --container-definitions "[{\"name\":\"app-$TEAM\",\"portMappings\":[{\"containerPort\": 5000,\"hostPort\":5000}],\"image\":\"${NEW_IMAGE}\",\"cpu\":0,\"memory\":200,\"essential\":true}]"

aws ecs update-service \
  --cluster FORM-UDD-$TEAM-cluster \
  --service FORM-UDD-$TEAM-app \
  --task-definition FORM-UDD-"$TEAM"_app \
  --force-new-deployment \
  --region $AWS_REGION
