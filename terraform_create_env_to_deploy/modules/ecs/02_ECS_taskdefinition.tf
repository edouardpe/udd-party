
/*====
ECS task definitions
======*/

/* the task definition for the app service */
data "template_file" "app_task" {
  template = "${file("${path.module}/tasks/app_task_definition.json")}"

  vars {
    application        = "${var.application}"
    imagelocal         = "${var.image}"
    image_publish_port = "${var.image_publish_port}"
    regionlocal        = "${var.region}"
    database_url       = "postgresql://${var.database_username}:${var.database_password}@${var.database_endpoint}:5432/${var.database_name}?encoding=utf8&pool=40"
    log_group          = "${aws_cloudwatch_log_group.jobs.name}"
  }
}

resource "aws_ecs_task_definition" "app" {
  family                   = "${var.environment}_app"
  container_definitions    = "${data.template_file.app_task.rendered}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "256"
  memory                   = "512"
  execution_role_arn       = "${aws_iam_role.ecs_execution_role.arn}"
  task_role_arn            = "${aws_iam_role.ecs_execution_role.arn}"
}

/* Simply specify the family to find the latest ACTIVE revision in that family */
data "aws_ecs_task_definition" "app" {
  depends_on = [ "aws_ecs_task_definition.app" ]
  task_definition = "${aws_ecs_task_definition.app.family}"
}
