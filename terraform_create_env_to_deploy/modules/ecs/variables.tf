variable "name" {
  description = "The name of module"
}

variable "environment" {
  description = "The environment"
}

variable "trigramme" {
  description = "trigramme of creator"
}

variable "region" {
  description = "Region that the instances will be created"
}

variable "vpc_id" {
  description = "The VPC id"
}

variable "availability_zones" {
  type        = "list"
  description = "The azs to use"
}

variable "security_groups_ids" {
  type        = "list"
  description = "The SGs to use"
}

variable "subnets_ids" {
  type        = "list"
  description = "The private subnets to use"
}

variable "public_subnet_ids" {
  type        = "list"
  description = "The private subnets to use"
}

variable "database_endpoint" {
  description = "The database endpoint"
  default = "database_endpoint"
}

variable "database_username" {
  description = "The database username"
  default = "database_username"
}

variable "database_password" {
  description = "The database password"
  default = "database_password"
}

variable "database_name" {
  description = "The database that the app will use"
  default = "database_name"
}


variable "application" {
  description = "The application name of the app"
}

variable "application_health_check_path" {
  description = "The application health check path"
}

variable "image" {
  description = "The image link of the app"
}

variable "image_publish_port" {
  description = "The port publish by the app"
}
