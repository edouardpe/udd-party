/*====
Cloudwatch Log Group
======*/
resource "aws_cloudwatch_log_group" "jobs" {
  name = "${var.application}"

  tags {
    Trigramme   = "${var.trigramme}"
    Environment = "${var.environment}"
    Application = "${var.application}"
  }
}
