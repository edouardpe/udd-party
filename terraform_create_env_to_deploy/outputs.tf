output "alb_dns_name_team1" {
  value = "${module.ecs_team1.alb_dns_name}"
}


output "alb_dns_name_team2" {
  value = "${module.ecs_team2.alb_dns_name}"
}
