/*WARNING:  Fargate isn't available on all regions
   see https://aws.amazon.com/fr/about-aws/global-infrastructure/regional-product-services/
*/
region                        = "eu-west-1"
key_name                      = "cypa-key"
/* WARNING: choose short env name :
    expected length of name to be in the range (0 - 32), got $environment-alb-target-group-da08
*/
environment                   = "FORM-UDD"
trigramme                     = "CYPA"

/* ECS */
application                   = "app"
image                         = "registry.gitlab.com/cparisot/formation_ci-cd:latest"
image_publish_port            = "5000"

/* application */
application_health_check_path = "/"
