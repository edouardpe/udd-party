/*====
Variables used across all modules
======*/
locals {
  availability_zones = ["${var.region}a", "${var.region}b"]
}

provider "aws" {
  region  = "${var.region}"
}


module "networking" {
  source               = "./modules/networking"
  environment          = "${var.environment}"
  trigramme            = "${var.trigramme}"
  vpc_cidr             = "10.0.0.0/16"
  public_subnets_cidr  = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets_cidr = ["10.0.10.0/24", "10.0.20.0/24"]
  region               = "${var.region}"
  availability_zones   = "${local.availability_zones}"
  key_name             = "${var.key_name}"
}

#need multiple module sinc module dont provide count (wait for v0.12 )
module "ecs_team1" {
  name                  = "ECS-module-ecs_team1"
  source                = "./modules/ecs"
  environment           = "${var.environment}-team1"
  trigramme             = "${var.trigramme}"
  region                = "${var.region}"
  vpc_id                = "${module.networking.vpc_id}"
  availability_zones    = "${local.availability_zones}"
  subnets_ids           = ["${module.networking.private_subnets_id}"]
  public_subnet_ids     = ["${module.networking.public_subnets_id}"]
  security_groups_ids = [
    "${module.networking.security_groups_ids}"
  ]
  application                   = "${var.application}-team1"
  image                         = "${var.image}"
  application_health_check_path = "${var.application_health_check_path}"
  image_publish_port            = "${var.image_publish_port}"
  }

  module "ecs_team2" {
    name                  = "ECS-module-ecs_team2"
    source                = "./modules/ecs"
    environment           = "${var.environment}-team2"
    trigramme             = "${var.trigramme}"
    region                = "${var.region}"
    vpc_id                = "${module.networking.vpc_id}"
    availability_zones    = "${local.availability_zones}"
    subnets_ids           = ["${module.networking.private_subnets_id}"]
    public_subnet_ids     = ["${module.networking.public_subnets_id}"]
    security_groups_ids = [
      "${module.networking.security_groups_ids}"
    ]
    application                   = "${var.application}-team2"
    image                         = "${var.image}"
    application_health_check_path = "${var.application_health_check_path}"
    image_publish_port            = "${var.image_publish_port}"
    }
