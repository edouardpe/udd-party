# Infra as code by Terraform

#How to create or update aws infra

## Create your infra

1. First, you have to be connected to aws


  test AWS connection
  ```
  aws sts get-caller-identity
  ```

2. Move to terraform directory

	```
	cd terraform_create_env_to_deploy/
	```

3. Load terraform module

	```
	terraform init
	```
4. Plannification of ressources
	*check env_test/terraform.tfvars for main custom variables*
	```
	terraform plan
	```

5. Apply ressources

	```
	terraform apply  
	```

## Destroy your infra

  ```
  terraform destroy
  ```


## Archi created
![alt text][Architecture_deploy]
[Architecture_deploy]: img/archi.png

This teraform is compose of 3 Modules
## Module network
This module create a VPC with 2 subnets (1 public and 1 private) in each Availability Zone.
Keeping our resources in more than one zone is the first thing to do, to achieve high availability.
If one physical zone fails, our application can answer from the others.


The private subnet is allowed only to be accessed from resources inside the public network In our case, will be the Load Balancer only.

## Module ecs (Fargate)
### Fargate (ECS) reminder
AWS Fargate is a technology for Amazon ECS that allows to run containers without having to manage servers or clusters.

- **Cluster** : It is a group of EC2 instances hosting containers.
- **Task definition**: It is the specification of how ECS should run your app. Here you define which image to use, port mapping, memory, environments variables, etc.
- **Service**: Services launches and maintains tasks running inside the cluster. A Service will auto-recover any stopped tasks keeping the number of tasks running as you specified.

### Module action
This module provision ECS cluster, create a task definition of api-backend.
It create the load balancers on the public subnet and will forward the requests to the ECS service

#### Autoscaling
This module handle auto-scaling via metrics in CloudWatch and trigger to scale it up or down.
If the CPU usage is greater than 85% from 2 periods, we trigger the alarm_action that calls the scale-up policy. If it returns to the Ok state, it will trigger the scale-down policy.

## license

This project is licensed under the WTFPL License - see the LICENSE file for details

![wtfpl](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)
