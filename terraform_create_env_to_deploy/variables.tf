variable "region" {
  description = "Region that the instances will be created"
}

variable "key_name" {
  description = "Name of your AWS-Cli profile"
}

/*====
environment specific variables
======*/

variable "environment" {
  description = "The environment"
}

variable "trigramme" {
  description = "The port publish by the app"
}


variable "domain" {
  default = "The domain of your application"
}

variable "application" {
  description = "The application name of the app"
}

variable "application_health_check_path" {
  description = "The application health check path"
}

variable "image" {
  description = "The image link of the app"
}

variable "image_publish_port" {
  description = "The port publish by the app"
}

variable "number_of_team" {
  description = "nomber of ecs cluster to create"
    default =1
}
